﻿using ShapesV2;
using SixLabors.ImageSharp;
using Rectangle = ShapesV2.Rectangle;

namespace DrawShapesV2
{
    class Program
    {
        static void Main(string[] args)
        {
            string docs = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string image_file = "ex4.png";
            // string filename = docs + "/" + image_file;
            string filename = Path.Combine(docs, image_file);
            
            Canvas canvas = new Canvas(1000,1000, Color.Black);
            /*
            ShapesV2.Point p1 = new ShapesV2.Point(100, 50, Color.RebeccaPurple);
            p1.Draw(canvas);
            p1.X = 200;
            p1.Draw(canvas);
            p1.Y = 25;
            p1.DrawColor = Color.Chartreuse;
            p1.Draw(canvas);
            Console.WriteLine($"Writing image in file: {filename}");
            canvas.Save(filename);
            
            ShapesV2.Point p5 = new ShapesV2.Point(2, 4);
            ShapesV2.Point p6 = new ShapesV2.Point(2, 3);
            Console.WriteLine($"p5 == p6? {p5 == p6}");
            Console.WriteLine($"p5.Equals(p6)? {p5.Equals(p6)}");
            p6.Y = 4;
            Console.WriteLine($"p5 == p6? {p5 == p6}");
            Console.WriteLine($"p5.Equals(p6)? {p5.Equals(p6)}");
            */
            
            ShapesV2.Point p1 = new ShapesV2.Point(200, 250, Color.Chartreuse);
            ShapesV2.Point p2 = new ShapesV2.Point(500, 250, Color.Chartreuse);
            ShapesV2.Point p3 = new ShapesV2.Point(400, 150, Color.Chartreuse);
            

            VLine vLine1 = new VLine(p1, 75, Color.Chartreuse);
           // vLine1.Draw(canvas);

           HLine hLine1 = new HLine(p1, 75, Color.Chartreuse);
           // hLine1.Draw(canvas);

           Line line1 = new Line(p1, p2, Color.Chartreuse);
           // line1.Draw(canvas);

           Rectangle rec1 = new Rectangle(p1, 150, 150, Color.Chartreuse);
           // rec1.Draw(canvas);

           Triangle t1 = new Triangle(p1, p2, p3, Color.Aqua);
           //t1.Draw(canvas);
           
           Circle c1 = new Circle(p1, 50, Color.Aqua); 
           Random random = new Random();
            for (int i = 1; i < 250; i+=25)
            {
              c1.Radius = i;
              //c1.Draw(canvas);
              c1.DrawColor = Color.FromRgb(r:(byte)random.Next(),(byte)random.Next(),(byte)random.Next());
            }
            Console.WriteLine($"Writing image in file: {filename}");
            canvas.Save(filename);
        }
    }
}