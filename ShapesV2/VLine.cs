using SixLabors.ImageSharp;

namespace ShapesV2;

public class VLine
{
    public Point StarPoint { get; set; }
    public int Height { get; set; }
    public Color DrawColor { get; set; }


    public VLine(Point starPoint,int height,Color drawColor)
    {
        StarPoint = starPoint;
        Height = height;
        DrawColor = drawColor;
    }

    public void Draw(Canvas canvas)
    {
        if (StarPoint.X >= 0 && StarPoint.X < canvas.Width)
        {
            for (int j = Math.Max(StarPoint.Y, 0); j < StarPoint.Y + Height && j < canvas.Height; j++)
            {
                canvas.SetPixel(StarPoint.X, j, DrawColor);
            }
        }
    }
}