using SixLabors.ImageSharp;

namespace ShapesV2;

public class Triangle
{
    public Point FirstPoint { get; set; }
    public Point SecondPoint { get; set; }
    public Point ThirdPoint { get; set; }
    public Color DrawColor { get; set; }

    public Triangle(Point firstPoint, Point secondPoint, Point thirdPoint, Color drawColor)
    {
        FirstPoint = firstPoint;
        SecondPoint = secondPoint;
        ThirdPoint = thirdPoint;
        DrawColor = drawColor;
    }

    public void Draw(Canvas canvas)
    {
        Line l1 = new Line(FirstPoint, SecondPoint, DrawColor);
        l1.Draw(canvas);
        l1.EndPoint = ThirdPoint;
        l1.Draw(canvas);
        l1.StartPoint = SecondPoint;
        l1.Draw(canvas);
    }
}