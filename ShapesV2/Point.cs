﻿using SixLabors.ImageSharp;

namespace ShapesV2;

public class Point
{
    public static Color DefaultDrawColor => Color.Black;
    
    private int _x;
    private int _y;

    public int X
    {
        get => _x;
        set
        {
            if (value < 0)
            {
                throw new ArgumentException("X cannot be negative");
            }

            _x = value;
        }
    }

    public int Y
    {
        get => _y;
        set
        {
            if (value < 0)
            {
                throw new ArgumentException("Y cannot be negative");
            }

            _y = value;
        }
    }

    public Color DrawColor { get; set; }

    public Point(int x, int y) : this(x, y, DefaultDrawColor) //this constructor is used if no color is put in as arguments
    {
    }

    public Point(int x, int y, Color drawColor)
    {
        X = x;
        Y = y;
        DrawColor = drawColor;
    }

    public Point(Point other) : this(other.X, other.Y, other.DrawColor)
    {
    }

    public override bool Equals(object? obj)
    {
        if (obj == null || GetType() != obj.GetType()) //if obj is null or the type of this.GetType is not the same as obj.GetType
        {
            return false;
        }

        Point other = (Point)obj; // force a conversion of Point into object or the opposite not sure
        return X == other.X && Y == other.Y; // this is the line that truly compares
    }

    public override int GetHashCode() // can generate from right click // 
    {
        return HashCode.Combine(X, Y);
    }

    public override string ToString()
    {
        return $"({X}, {Y})";
    }

    public int Length()
    {
        double length = Math.Sqrt(X * X + Y * Y);
        // return length;
        return (int)Math.Round(length, MidpointRounding.AwayFromZero);
    }

    public void ScaleX(double factor)
    {
        X = (int)Math.Round(X * factor, MidpointRounding.AwayFromZero);
    }

    public void ScaleY(double factor)
    {
        Y = (int)Math.Round(Y * factor, MidpointRounding.AwayFromZero);
    }

    public void Scale(double factor)
    {
        ScaleX(factor);
        ScaleY(factor);
    }

    public Point MidPoint()
    {
        Point temp = new Point(this);
        temp.Scale(0.5);
        return temp;
    }

    public void Draw(Canvas canvas)
    {
        canvas[X, Y] = DrawColor;
    }
}